%define name pdftools.pdfjoin
%define version 0.1dev
%define unmangled_version 0.1dev
%define unmangled_version 0.1dev
%define release 1

Summary: Join PDF documents into a single document
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: GPLv3
Group: Publishing
Prefix: %{_prefix}
BuildArch: noarch
Vendor: Hartmut Goebel <h.goebel@goebel-consult.de>
Url: https://pypi.python.org/pypi/pdftools.pdfjoin

BuildRequires: scons


%description

``pdfjoin`` can be used to join PDF documents. It supports a simple
``droplet`` mode. It will join all files passed as argument into a new
file.

``pdfjoin`` has three modes of operation:

1) With no output path given: The droplet mode.

   If no output path is passed to ``pdfjoin``, will look for a file
   matching ``pdfjoin-*.pdf`` in the current directory, which has been
   modified within the last 60 minutes. (More precise, the filename
   must match the scheme as described in the next paragraph.)q If such
   a file exists, all documents passed as arguments will be joined to
   this one.

   If such a file does not exist, it will be created first. The
   filename will be ``pdfjoin-yyyddmm-hhmm.pdf`` with ``yyyddmm``
   being the current date and ``hhmm`` being the current time.

   This allows dropping one file after each other onto the droplet and
   join them into the same output file -- assuming you are not waiting
   more then 60 Minutes between each drop.

2) With an existing directory name is given as output path

   This case works exactly like the droplet mode, except that the file
   is searched for and created in the given directory.

3) With any other path given as output path:

   In this case the given path is the filename searched for and
   created if not existing yet.



%prep
%setup -q

%build
python setup.py build
scons doc

%install
python setup.py install --single-version-externally-managed -O1 \
   --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

#xdg-desktop-icon pdfjoin.desktop
mkdir -p %{buildroot}%{_datadir}/dist/desktop-files
install -m 644 pdfjoin.desktop %{buildroot}%{_datadir}/dist/default/desktop-files/pdfjoin-droplet.desktop
install -m 644 -D droplet.svg %{buildroot}%{_iconsscaldir}/pdfjoin-droplet.svg

# man-page
install -m 644 -D doc/pdfjoin.1 %{buildroot}%{_mandir}/man1/pdfjoin.1


%post
update-desktop-database &> /dev/null || :

%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc README.txt doc/*.html
%{_datadir}/dist/desktop-files/*/*.desktop
%{_iconsscaldir}/*.svg
%{_mandir}/*/*
