
General Options
--------------------

--version        Show program's version number and exit
-h, --help       Show help message and exit
-v, --verbose    Be verbose. Tell about scaling, rotation and number of
                 pages. Can be used more than once to increase the
                 verbosity.
-n, --dry-run    Show what would have been done, but do not generate files.

Defining Output
-----------------

-o OUTPUT, --output=OUTPUT
                 Specify filename or directory to write file to
                 (default: current directory)

.. Emacs config:
 Local Variables:
 mode: rst
 End:
