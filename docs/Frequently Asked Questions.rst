Frequently Asked Questions
===============================

* Are there other Python tools for manipulating PDF?

  Yes, there are:

  * `pdfposter <https://pdfposter.readthedocs.io/>`_
  * `pdfdecrypt <https://pdfdecrypt.readthedocs.io/>`_
  * `pdfbook <https://pdfbook.readthedocs.io/>`_
  * `flyer-composer <http://www.crazy-compilers.com/flyer-composer.html>`_
    (including an optional Qt GUI)

  * `pdfnup <https://pypi.org/project/pdfnup/>`_
  * `pdfsplit <https://pypi.org/project/pdfsplit/>`_
  * `pdfgrid <https://pypi.org/project/pdfgrid/>`_


.. include:: _common_definitions.txt
