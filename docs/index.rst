==============================================
pdfjoin
==============================================

.. container:: admonition topic

  **Join several PDF documents into a single one - includes droplet.**

``pdfjoin`` is a command line tool, a droplet and a Python library to
join several PDF documents into a single one. It will join all files
passed as argument into a new file. As a special feature it can
operate as a "droplet".


.. toctree::
   :maxdepth: 1

   Installation
   Usage
   Examples
   Donate <Donate>
   Frequently Asked Questions
   Changes
   Development

.. include:: _common_definitions.txt
