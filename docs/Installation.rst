
Download & Installation
=========================

Instructions for Windows Users
-----------------------------------

1. |pdfjoin| requires Python. If you don't have Python installed already,
   download and install Python 3.11 from https://python.org/download/3.11/

   During installation, make sure to check "Include into PATH".

2. If you already have Python installed, please check that your Python
   directory (normally :file:`C:\\Python311` for python 3.11) and the Python
   Scripts directory (normally :file:`C:\\Python311\\Scripts`) are in the system
   path. If not, just add them in :menuselection:`My Computer --> Properties
   --> Advanced --> Environment Variables` to the :envvar:`Path` system
   variable.

3. Install |pdfjoin| by running ::

     pip install pdfjoin

   Then run the console command ``pdfjoin --help`` to get detailed help.

   If the command ``pip`` is unknown to you system, please refer to the
   `pip homepage <https://pip.pypa.io/en/stable/installing/>`_ for help.


Instructions for GNU/Linux and other Operating Systems
--------------------------------------------------------

Most current GNU/Linux distributions provide packages for |pdfjoin|.
Simply search your distribution's software catalog.

Also many vendors provide Python, and some even provide |pdfjoin|.
Please check your vendor's software repository.

If your distribution or vendor does not provide a current version of
|pdfjoin| please read on.

If your vendor does not provide :command:`python`
please download Python 3.11 from https://www.python.org/download/ and
follow the installation instructions there.

If you distribution or vendor missed providing :command:`pip`,
alongside :command:`python`,
please check your vendor's or distribution's software repository
for a package called `pip`, `python3-pip` or `python-pip`.
If this is not provided, please refer to the
`pip homepage <https://pip.pypa.io/en/stable/installing/>`_ for help.


Optionally you might want to install `PyPDF2`
- which is a requirement for |pdfjoin| -
provided by your distribution or vendor
so at least this package will be maintained by your distribution.
Check for a package named `python3-pypdf2` or that like.

Then continue with :ref:`installing pdfjoin` below.


.. _installing pdfjoin:

Installing |pdfjoin| using :command:`pip`
---------------------------------------------

After installing `Python` (and optionally `PyPDF2`), just run::

  sudo pip install pdfjoin

to install |pdfjoin| for all users.
For installing |pdfjoin| for yourself only, run::

  pip install --user pdfjoin

If your system does not have network access

- download |pdfjoin| from https://pypi.org/project/pdfjoin/,

- downlaod `PyPDF2` from https://pypi.org/project/PyPDF2/, and

- run ::

    sudo pip install pdfjoin-*.tar.gz PyPDF2-*.tar.gz

  respective ::

    pip install --user pdfjoin-*.tar.gz PyPDF2-*.tar.gz


.. include:: _common_definitions.txt

