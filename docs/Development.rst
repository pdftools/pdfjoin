Development
===============================

The source of |pdfjoin| and its siblings is maintained at
a decentralized `GitLab` instance. Patches and pull-requests
are hearty welcome.

* Please submit bugs and enhancements to the `Issue Tracker
  <https://gitlab.digitalcourage.de/pdftools/pdfjoin/issues>`_.

* You may browse the code at the
  `Repository Browser
  <https://gitlab.digitalcourage.de/pdftools/pdfjoin>`_.
  Or you may check out the current version by running ::

    git clone https://gitlab.digitalcourage.de/pdftools/pdfjoin.git


.. include:: _common_definitions.txt
