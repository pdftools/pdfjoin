"""
``pdfjoin`` can be used to join PDF documents. It supports a simple
``droplet`` mode. It will join all files passed as argument into a new
file.

``pdfjoin`` has three modes of operation:

1) With no output path given: The droplet mode.

   If no output path is passed to ``pdfjoin``, will look for a file
   matching ``pdfjoin-*.pdf`` in the current directory, which has been
   modified within the last 60 minutes. (More precise, the filename
   must match the scheme as described in the next paragraph.)q If such
   a file exists, all documents passed as arguments will be joined to
   this one.

   If such a file does not exist, it will be created first. The
   filename will be ``pdfjoin-yyyddmm-hhmm.pdf`` with ``yyyddmm``
   being the current date and ``hhmm`` being the current time.

   This allows dropping one file after each other onto the droplet and
   join them into the same output file -- assuming you are not waiting
   more then 60 Minutes between each drop.

2) With an existing directory name is given as output path

   This case works exactly like the droplet mode, except that the file
   is searched for and created in the given directory.

3) With any other path given as output path:

   In this case the given path is the filename searched for and
   created if not existing yet.

"""

from setuptools import setup, find_packages
additional_keywords ={}

from distutils.core import Command
from distutils import log
import os

class build_docs(Command):
    description = "build documentation from rst-files"
    user_options=[]

    def initialize_options (self): pass
    def finalize_options (self):
        self.docpages = DOCPAGES
        
    def run(self):
        substitutions = ('.. |VERSION| replace:: '
                         + self.distribution.get_version())
        for writer, rstfilename, outfilename in self.docpages:
            distutils.dir_util.mkpath(os.path.dirname(outfilename))
            log.info("creating %s page %s", writer, outfilename)
            if not self.dry_run:
                try:
                    rsttext = open(rstfilename).read()
                except IOError, e:
                    sys.exit(e)
                rsttext = '\n'.join((substitutions, rsttext))
                # docutils.core does not offer easy reading from a
                # string into a file, so we need to do it ourself :-(
                doc = docutils.core.publish_string(source=rsttext,
                                                   source_path=rstfilename,
                                                   writer_name=writer)
                try:
                    rsttext = open(outfilename, 'w').write(doc)
                except IOError, e:
                    sys.exit(e)

cmdclass = {}

try:
    import docutils.core
    import docutils.io
    import docutils.writers.manpage
    import distutils.command.build
    distutils.command.build.build.sub_commands.append(('build_docs', None))
    cmdclass['build_docs'] = build_docs
except ImportError:
    log.warn("docutils not installed, can not build man pages. "
             "Using pre-build ones.")

DOCPAGES = (
    ('manpage', 'pdfjoin.rst', 'doc/pdfjoin.1'),
    ('html', 'pdfjoin.rst', 'doc/pdfjoin.html'),
    )


setup(
    cmdclass=cmdclass,
    name = "pdftools.pdfjoin",
    version = "0.1dev",
    install_requires = ['pyPdf>1.10'],

    packages=find_packages(exclude=['ez_setup']),
    namespace_packages=['pdftools'],

    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        'hello': ['*.msg'],
        },

    # metadata for upload to PyPI
    author = "Hartmut Goebel",
    author_email = "h.goebel@crazy-compilers.com",
    description = "Join PDF documents into a single document.",
    long_description = __doc__,
    license = "GPL 3.0",
    keywords = "PDF,join,merge",
    url          = "https://gitlab.com/pdftools/pdfjoin",
    #download_url = "https://gitlab.com/pdftools/pdfjoin/",
    classifiers = [
    'Development Status :: 3 - Alpha', 
    'Environment :: Console',
    'Intended Audience :: Developers',
    'Intended Audience :: End Users/Desktop',
    'Intended Audience :: System Administrators',
    'License :: OSI Approved :: GNU General Public License (GPL)',
    'Natural Language :: English',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Topic :: Printing',
    'Topic :: Utilities',
    ],

    # these are for easy_install (used by bdist_*)
    zip_safe = True,
    entry_points = {
        "console_scripts": [
            "pdfjoin = pdftools.pdfjoin.cmd:run",
        ],
    },
    **additional_keywords
)
