==========================
pdfjoin
==========================
-------------------------------------------------------------
Join several PDF documents into a single one
-------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version:   Version |VERSION|
:Copyright: 2012-2023 by Hartmut Goebel
:Licence:   GNU Affero General Public License v3 or later (AGPLv3+)
:Manual section: 1

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l


SYNOPSIS
==========

``pdfjoin`` <options> infile ...

DESCRIPTION
============

.. include:: docs/_description.txt

OPTIONS
========

.. include:: docs/_options.txt


EXAMPLES
============

.. include:: docs/_examples.txt


SEE ALSO
=============

``pdfposter``\(1) https://pdfposter.readthedocs.io/,
``pdfbook``\(1) https://pdfbook.readthedocs.io/,
``pdfdecrypt``\(1) https://pdfdecrypt.readthedocs.io/,
``pdfnup``\(1) https://pypi.python.org/pypi/pdfnup/,
``pdfsplit``\(1) https://pypi.python.org/pypi/pdfsplit/,
``pdfgrid``\(1) https://pypi.python.org/pypi/pdfgrid/,
``flyer-composer``\(1) http://www.crazy-compilers.com/flyer-composer.html

Project Homepage https://pdfjoin.readthedocs.io/
