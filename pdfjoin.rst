.. -*- mode: rst ; ispell-local-dictionary: "american" -*-

==========================
pdfjoin
==========================

-------------------------------------------------------------
Join several PDF documents into a single one
-------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version:   Version |VERSION|
:Copyright: 2008-2011 by Hartmut Goebel
:Licence:   GNU Public Licence v3 (GPLv3)
:Manual section: 1

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l


SYNOPSIS
==========

``pdfjoin`` <options> infile ...

DESCRIPTION
============

``pdfjoin`` is a command line tool, a droplet and a Python library to
join several PDF documents into a single one. It will join all files
passed as argument into a new file. As a special feature it can
operate as a "droplet".

``pdfjoin`` has three modes of operation:

1) With no output path given: The droplet mode.

   If no output path is passed to ``pdfjoin``, will look for a file
   matching ``pdfjoin-*.pdf`` in the current directory, which has been
   modified within the last 60 minutes. (More precise, the filename
   must match the scheme as described in the next paragraph.)q If such
   a file exists, all documents passed as arguments will be joined to
   this one.

   If such a file does not exist, it will be created first. The
   filename will be ``pdfjoin-yyyddmm-hhmm.pdf`` with ``yyyddmm``
   being the current date and ``hhmm`` being the current time.

   This allows dropping one file after each other onto the droplet and
   join them into the same output file -- assuming you are not waiting
   more then 60 Minutes between each drop.

2) With an existing directory name is given as output path

   This case works exactly like the droplet mode, except that the file
   is searched for and created in the given directory.

3) With any other path given as output path:

   In this case the given path is the filename searched for and
   created if not existing yet.


OPTIONS
========

General Options
--------------------

--version        Show program's version number and exit
-h, --help       Show help message and exit
-v, --verbose    Be verbose. Tell about scaling, rotation and number of
                 pages. Can be used more than once to increase the
                 verbosity.
-n, --dry-run    Show what would have been done, but do not generate files.

Defining Output
-----------------

-o OUTPUT, --output=OUTPUT
                 Specify filename or directory to write file to
                 (default: current directory)


EXAMPLES
============

:pdfjoin infile1.pdf infile2.pdf infile3.pdf:
       Creates a file named like ``pdfjoin-20120102-1415.pdf`` in the
       current directory.

:pdfjoin infile1.pdf ; pdfjoin infile2.pdf ; pdfjoin infile3.pdf:
       Same as above -- assuming the deplay between issuing each of
       the commands is less than 60 minutes.

:pdfjoin infile1.pdf infile2.pdf infile3.pdf --output /tmp/foo:
       Creates a file named like
       ``/tmp/foo/pdfjoin-20120102-1415.pdf`` in the current
       directory. But only if /tmp/foo already exisits!

:pdfjoin infile1.pdf --output /tmp/foo; pdfjoin infile3.pdf --output /tmp/foo:
       Same as above -- again assuming the deplay between issuing each
       of the commands is less than 60 minutes.

:pdfjoin infile1.pdf infile2.pdf infile3.pdf --output /tmp/foo.pdf:
       Creates a file named like ``/tmp/foo.pdf`` in the current
       directory. But only if /tmp/foo.pdf is no exiting directory!


SEE ALSO
=============

``pdfposter``\(1) http://pypi.python.org/pypi/pdftools.pdfposter/,
``pdfnup``\(1) http://pypi.python.org/pypi/pdfnup/,
``pdfsplit``\(1) http://pypi.python.org/pypi/pdfsplit/,
``pdfgrid``\(1) http://pypi.python.org/pypi/pdfgrid/

Project Homepage http://gitlab.com/pdftools/pdfjoin
