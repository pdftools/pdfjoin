.. -*- mode: rst ; ispell-local-dictionary: "american" -*-

================================================
How-to Setup a `pdfjoin` Development environment
================================================

Create a virtual environment (including distribute) somewhere. Lets
call it `venv`::

    virtualenv --distribute ./venv

Activate our environment::

    source ./venv/bin/activate

Run the develop script::

    python setup.py develop

Going back to your non-virtual environment::

    deactivate

Start squeal (no need to activate the virtual environment)::

    venv/bin/pdfjoin ...
