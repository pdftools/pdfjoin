.. -*- mode: rst ; ispell-local-dictionary: "american" -*-

==========================
pdfjoin
==========================

-----------------------------------------------------------------
Join several PDF documents into a single one -- includes droplet
-----------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version:   Version 0.1dev
:Copyright: 2012 by Hartmut Goebel
:Licence:   GNU Public Licence v3 (GPLv3)
:Homepage:  http://gitlab.com/pdftools/pdfjoin

``pdfjoin`` is a command line tool, a droplet and a Python library to
join several PDF documents into a single one. It will join all files
passed as argument into a new file. As a special feature it can
operate as a "droplet".

``pdfjoin`` has three modes of operation:

1) With no output path given: The droplet mode.

   If no output path is passed to ``pdfjoin``, will look for a file
   matching ``pdfjoin-*.pdf`` in the current directory, which has been
   modified within the last 60 minutes. (More precise, the filename
   must match the scheme as described in the next paragraph.)q If such
   a file exists, all documents passed as arguments will be joined to
   this one.

   If such a file does not exist, it will be created first. The
   filename will be ``pdfjoin-yyyddmm-hhmm.pdf`` with ``yyyddmm``
   being the current date and ``hhmm`` being the current time.

   This allows dropping one file after each other onto the droplet and
   join them into the same output file -- assuming you are not waiting
   more then 60 Minutes between each drop.

2) With an existing directory name is given as output path

   This case works exactly like the droplet mode, except that the file
   is searched for and created in the given directory.

3) With any other path given as output path:

   In this case the given path is the filename searched for and
   created if not existing yet.


Requirements and Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``pdfjoin`` requires

* `Python 2.x`__ or higher (tested with 2.7, but other versions should
  work, too, Python 3.x is *not* supported),
* `setuptools`__ for installation (see below), and
* `pyPdf`__ >= 1.11.

__ http://www.python.org/download/
__ http://pypi.python.org/pypi/setuptools
__ http://pybrary.net/pyPdf/


:Hints for installing on Windows: Following the links above you will
   find .msi and .exe-installers. Simply install them and continue
   with `installing pdfjoin`_.

:Hints for installing on GNU/Linux: Most current GNU/Linux distributions
   provide packages for the requirements. Look for packages names like
   `python-setuptools` and `python-pypdf`. Simply install them and
   continue with `installing pdfjoin`_.

:Hint for installing on other platforms: Many vendors provide Python.
   Please check your vendors software repository. Otherwise please
   download Python 2.6 (or any higer version from the 2.x series) from
   http://www.python.org/download/ and follow the installation
   instructions there.

   After installing Python, install `setuptools`__. You may want to
   read `More Hints on Installing setuptools`_ first.

__ http://pypi.python.org/pypi/setuptools

   Using setuptools, compiling and installing the remaining
   requirements is a piece of cake::

     # if the system has network access
     easy_install pyPdf

     # without network access download pyPdf
     # from http://pybrary.net/pyPdf/ and run
     easy_install pyPdf-*.zip


Installing pdfjoin
---------------------------------

When you are reading this you most probably already downloaded and
unpacked `pdfjoin`. Thus installing is as easy as running::

   python ./setup.py install

Otherwise you may install directly using setuptools/easy_install. If
your system has network access installing `pdfjoin` is a
breeze::

     easy_install pdftools.pdfjoin

Without network access download `pdftools.pdfjoin` from
http://pypi.python.org/pypi/pdfjoin and run::

     easy_install pdftools.pdfjoin-*.tar.gz


More Hints on Installing setuptools
------------------------------------

`pdfjoin` uses setuptools for installation. Thus you need
either

  * network access, so the install script will automatically download
    and install setuptools if they are not already installed

or

  * the correct version of setuptools preinstalled using the
    `EasyInstall installation instructions`__. Those instructions also
    have tips for dealing with firewalls as well as how to manually
    download and install setuptools.

__ http://peak.telecommunity.com/DevCenter/EasyInstall#installation-instructions


Custom Installation Locations
------------------------------

If you want to install ``pdfjoin`` to a different place, simply use::

   # install to /usr/local/bin
   python ./setup.py install --prefix /usr/local

   # install to your Home directory (~/bin)
   python ./setup.py install --home ~


Please mind: This effects also the installation of pyPdf (and
setuptools) if they are not already installed.

For more information about Custom Installation Locations please refer
to the `Custom Installation Locations Instructions`__ before
installing ``pdfjoin``.

__ http://peak.telecommunity.com/DevCenter/EasyInstall#custom-installation-locations>
