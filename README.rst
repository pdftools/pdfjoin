==========================
pdfjoin
==========================

-----------------------------------------------------------------
Join several PDF documents into a single one -- includes droplet
-----------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version:   Version 0.1dev
:Copyright: 2012-2023 by Hartmut Goebel
:Licence:   GNU Affero General Public License v3 or later (AGPLv3+)
:Homepage:  https://pdfjoin.readthedocs.io/

``pdfjoin`` is a command line tool, a droplet and a Python library to
join several PDF documents into a single one. It will join all files
passed as argument into a new file. As a special feature it can
operate as a "droplet".

``pdfjoin`` has three modes of operation:

1) With no output path given: The droplet mode.

   If no output path is passed to ``pdfjoin``, will look for a file
   matching ``pdfjoin-*.pdf`` in the current directory, which has been
   modified within the last 60 minutes. (More precise, the filename
   must match the scheme as described in the next paragraph.)q If such
   a file exists, all documents passed as arguments will be joined to
   this one.

   If such a file does not exist, it will be created first. The
   filename will be ``pdfjoin-yyyddmm-hhmm.pdf`` with ``yyyddmm``
   being the current date and ``hhmm`` being the current time.

   This allows dropping one file after each other onto the droplet and
   join them into the same output file -- assuming you are not waiting
   more then 60 Minutes between each drop.

2) With an existing directory name is given as output path

   This case works exactly like the droplet mode, except that the file
   is searched for and created in the given directory.

3) With any other path given as output path:

   In this case the given path is the filename searched for and
   created if not existing yet.


Requirements when Installating from Source
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you want to install `pdfjoin` from source, make sure you have the
following software installed:

* `Python 3`__  (tested with 3.8),
* `pip`__ for installation, and
* `PyPDF2`__ < 3.0.

__ https://www.python.org/download/
__ https://pypi.org/project/pip
__ https://pypi.org/project/PyPDF2


:Hints for installing on Windows: Following the links above you will
   find .msi and .exe-installers. Simply install them and continue
   with `installing pdfjoin`_.

:Hints for installing on GNU/Linux: Most current GNU/Linux distributions
   provide packages for the requirements. Look for packages names like
   `python-pypdf2`. Simply install them and
   continue with `installing pdfjoin`_.

:Hint for installing on other platforms: Many vendors provide Python.
   Please check your vendors software repository. Otherwise please
   download Python 3.8 (or any higher version from the 3.x series) from
   https://www.python.org/download/ and follow the installation
   instructions there.

   If the commands below fail due to module `pip` not found,
   please install it using::

     python -m ensurepip



Installing pdfjoin
---------------------------------

If your system has network access installing `pdfjoin`
is a breeze::

  pip install pdfjoin


If you  downloaded and unpacked `pdfjoin` just run::

  python -m pip install .


Without network access download `pdfjoin` from
https://pypi.python.org/pypi/pdfjoin and run::

   pip install pdfjoin-*.tar.gz


For more details like custom installation locations
please refer to
`the official end user guide for installing Python packages
<https://docs.python.org/3/installing/>`__.
