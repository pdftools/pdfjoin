#!/usr/bin/env python
"""
pdftools.pdfjoin - Join PDF documents into a single file.
"""
#
# Copyright 2012 by Hartmut Goebel <h.goebel@crazy-compilers.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

__author__ = "Hartmut Goebel <h.goebel@crazy-compilers.com>"
__copyright__ = "Copyright 2012 by Hartmut Goebel <h.goebel@crazy-compilers.com>"
__licence__ = "GNU General Public License version 3 (GPL v3)"
__version__ = "0.1dev"

# ignore some warnings for pyPDF < 1.13
import warnings
warnings.filterwarnings('ignore', "the sets module is deprecated")
warnings.filterwarnings('ignore', "the md5 module is deprecated")

from pyPdf.pdf import PdfFileWriter, PdfFileReader

import logging
from logging import log
import tempfile
import shutil

class DecryptionError(ValueError): pass

def join(outfilename, infilenames, dry_run=False):
    # use a temporary file since the outfilename may be an inputfile, too.
    tmpfh = tempfile.TemporaryFile()

    outpdf = PdfFileWriter()
    for infilename in infilenames:
        log(19, 'Reading %r', infilename)
        with open(infilename, 'rb') as infh:
            inpdf = PdfFileReader(infh)

            if inpdf.isEncrypted:
                log(16, 'File is encrypted')
                # try empty password first
                if not inpdf.decrypt('') and not inpdf.decrypt(password_hook()):
                    raise DecryptionError("Can't decrypt PDF. Wrong Password?")

            for page in inpdf.pages:
                outpdf.addPage(page)
            outpdf.write(tmpfh)

    if dry_run:
        log(20, 'dry-run is selected. Not writing to %r', outfilename)
    else:
        log(19, 'Writing %r', outfilename)
        # Copy the data to the real outfile using `shutil` module.
        # NB: Do not use `shutil.move()` as this would copy the
        # restrictive permisstions, too.
        tmpfh.flush()
        tmpfh.seek(0)
        with open(outfilename, 'wb') as outfh:
            shutil.copyfileobj(tmpfh, outfh)

def password_hook():
    import getpass
    return getpass.getpass()

def main(opts, outfilename, infilenames, password_hook=password_hook):
    logging.basicConfig(level=20-opts.verbose, format="%(message)s")
    join(outfilename, infilenames, dry_run=opts.dry_run)

