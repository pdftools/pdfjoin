#!/usr/bin/env python
"""
pdftools.join.cmd - Join PDF documents into a single file.
"""
#
# Copyright 2012 by Hartmut Goebel <h.goebel@crazy-compilers.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

__author__ = "Hartmut Goebel <h.goebel@crazy-compilers.com>"
__copyright__ = "Copyright 2012 by Hartmut Goebel <h.goebel@crazy-compilers.com>"
__licence__ = "GNU General Public License version 3 (GPL v3)"

from . import main, __version__, DecryptionError

import glob
import os
import re
import time

def make_outname(dirname):
    """
    Search for a file matching `pdfposter-YYYMMMDD-hhmm.pdf and last
    modified within the last hour. If this does not exist, create a
    new filename.
    """
    names = glob.glob(os.path.join(dirname, 'pdfjoin-*.pdf'))
    # keep only those matching the pattern `pdfposter-YYYMMMDD-hhmm.pdf`
    names = [(os.path.getmtime(n), n)
             for n in names
             if re.match('^pdfjoin-\d{8}-\d{4}\.pdf', os.path.basename(n))]
    names.sort()
    if names:
        mtime, outname = names[-1]
        if time.time() - mtime < 3600:
            return outname
    # no matching file found which has been last motified within the last hour
    return os.path.join(dirname,time.strftime('pdfjoin-%Y%m%d-%H%M.pdf'))
    

def run():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', metavar='FILE', nargs='+',
                        help='input-files to join into the new file')

    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='Be verbose. Can be used more than once to '
                             'increase the verbosity. ')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='Show what would have been done, but do not '
                             'generate files.')
    parser.add_argument('--version', action='version', version=__version__)

    group = parser.add_argument_group('Define Target')
    group.add_argument('-o', '--output',
                       help='Specify filename or directory to write file to '
                            '(default: current directory)')

    args = parser.parse_args()

    if not args.output:
        outfilename = make_outname('.')
    elif os.path.isdir(args.output):
        outfilename = make_outname(args.output)
    else:
        outfilename = args.output

    if os.path.exists(outfilename):
        args.infiles.insert(0, outfilename)

    try:
        main(args, outfilename, args.infiles)
    except (IOError, DecryptionError), e:
        raise SystemExit(str(e))


if __name__ == '__main__':
    run()
